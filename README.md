# Zoomsense Plugin - Voting

ZoomSense Voting Plugin enables tutors to select strategies submitted by students for voting using the tutor dashboard. Students are expected to vote for or against a strategy using Y/y or N/n. The plugin aggregates the votes and allows tutors to display the strategies that have been most voted for in a ranked list on the overlay. It also allows the tutor to add new strategies from the dashboard and manipulate votes for strategies if required. 

## Local Development

Once the `.runtimeconfig.json` file is prepared under the `/functions` folder, Firebase Functions can be tested locally using the command:

```
npm run emulators
```

This ensures that the emulator data will be imported when Firebase Emulator starts and exported when the session ends.
```
