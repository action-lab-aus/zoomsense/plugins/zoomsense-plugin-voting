import * as admin from 'firebase-admin';

admin.initializeApp();

export * from './captureVotes';
export * from './captureChatSubmissions';
