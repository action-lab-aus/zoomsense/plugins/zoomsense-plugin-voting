import "@zoomsense/zoomsense-firebase";
import { Data as ChatSubmissionData } from "@zoomsense/chat-submission-types";
import {
  VotingPluginConfig,
  VotingPluginCurrentData,
} from "./votingPluginTypes";

declare module "@zoomsense/zoomsense-firebase" {
  export interface ZoomSenseConfigCurrentStatePlugins {
    voting: VotingPluginConfig;
  }

  export interface ZoomSenseDataPlugins {
    chatSubmission: ChatSubmissionData;
    voting: VotingPluginCurrentData;
  }
}

export const defaultSubmissionName = "strategy";

export const defaultVoteType = "yes-no";

export const voteTypeValidValues = {
  [defaultVoteType]: {
    n: "no",
    no: "no",
    y: "yes",
    yes: "yes",
  },
  rating: {
    1: "rating1",
    2: "rating2",
    3: "rating3",
    4: "rating4",
    5: "rating5",
  },
};
